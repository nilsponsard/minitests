#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <cassert>
#include <random>
#include <time.h>
using namespace std;

//exercice 1
void f(const string &str, const unsigned &i)
{
  if (str.size() != i)
  {
    cout << str[i];
    f(str, i + 1);
  }
}
void f(const string &str)
{
  f(str, 0);
  cout << endl;
}
void f2(const string &str)
{
  for (const char c : str)
  {
    cout << c;
  }
  cout << endl;
}

// exercice 2
typedef map<string, float> mapPrix;
mapPrix prix;
void changePrix(const string &produit, const float &valeur)
{
  prix[produit] = valeur;
}
void exercice2()
{
  prix["foie gras"] = 35.50;
  prix["huitres"] = 15.35;
  prix["champagne"] = 41.99;
  prix["alka setzer"] = 5.25;
  cout << prix["foie gras"] << endl;
  changePrix("foie gras", 30.00);
  cout << prix["foie gras"] << endl;
}

//exercice 3
void g(const string &str, const unsigned &i)
{

  cout << str[i];
  if (i > 0)
    g(str, i - 1);
}

void g2(const string &str)
{
  g(str, str.size() - 1);
  cout << endl;
}

void g(const string &str)
{
  for (unsigned i(str.size()); i-- > 0;)
    cout << str[i];
  cout << endl;
} // g()

//exercice 4
unsigned h(const string &str, const char &c, const unsigned &i)
{
  if (str.size() == i)
    return str.size();
  if (str[i] == c)
    return i;
  return h(str, c, i + 1);
} // h ()

unsigned h(const string &str, const char &c)
{
  return h(str, c, 0);
}

unsigned h2(const string &str, const char &c)
{
  unsigned i(0);
  for (const char &car : str)
  {
    if (car == c)
      return i;
    ++i;
  }
  return str.size();
}

//exercice 5
unsigned sommeN(const unsigned &n)
{
  if (n == 0)
    return 0;
  else
    return sommeN(n - 1) + n;
}
void testSommeN()
{
  unsigned n;
  for (unsigned i(0); i < 100; ++i)
  {
    n = random() % 5000; // éviter de dépasser la limite de stockage d'un unsigned
    assert(sommeN(n) == (n * (n + 1)) / 2);
  }
  cout << endl;
  cout << "tout est bon" << endl;
  cout << endl;
}

//exercice 6
int sommeInverse(const vector<int> &v)
{
  int somme = 0;
  for (unsigned i(v.size()); i-- > 0;)
    somme += v[i];
  return somme;
} // sommeInverse()
int sommeInverse(const vector<int> &v, unsigned i)
{
  if (i > 0)
    return sommeInverse(v, i - 1) + v[i];
  else
    return v[i];
}
int sommeInverse2(const vector<int> &v)
{
  return sommeInverse(v, v.size() - 1);
}

//exercice 7
typedef map<float, string> prixProduitMap;
prixProduitMap mapProduit;
void initPrixProduit()
{
  mapProduit[35.50] = "foie gras";
  mapProduit[15.35] = "huitres";
  mapProduit[41.99] = "champagne";
  mapProduit[5.25] = "alka seltzer";
}
void changeMapProduit(const float &clef, const string &valeur)
{
  mapProduit[clef] = valeur;
}

// exercice 8
unsigned a(const string &str, const char &c)
{
  unsigned i(str.size());
  for (; i > 0; --i)
    if (str[i] == c)
      return i;
  return str[i] == c ? 0 : str.size();
} // a()
unsigned a(const string &str, const char &c, unsigned i)
{

  if (str[i] == c)
    return i;
  else
  {
    if (i == 0)
      return str.size();
    else
      return a(str, c, i - 1);
  }
}
unsigned a2(const string &str, const char &c)
{
  return a(str, c, str.size() - 1);
}

//exercice 9
typedef vector<int> photographie; // pour que ça compile
void afficher(const photographie &photo)
{
  cout << photo[0] << endl; // code de remplacement pour que ça compile
}
typedef map<string, photographie> photoMap;
void affichePhotos(const photoMap &photos)
{
  for (auto iter(photos.begin()); iter != photos.end(); ++iter)
  {
    cout << iter->first << endl;
    afficher(iter->second);
    char c('-');
    while (c != '\n')
    {
      c = getchar();
    }
  }
}

//exercice 10
int sommeVect(const vector<int> &v, const unsigned &i)
{
  if (i == v.size())
    return 0;
  return v[i] + sommeVect(v, i + 1);
} // sommeVect()
int sommeVect(const vector<int> &v)
{
  return sommeVect(v, 0);
}
int sommeVect2(const vector<int> &v)
{
  int somme(0);
  for (const int i : v)
    somme = somme + i;
  return somme;
}
void testSommeVect2()
{
  vector<int> v({8, 5, 2, 19, 8, 4});
  assert(sommeVect2(v) == 8 + 5 + 2 + 19 + 8 + 4);
  cout << "tout est bon" << endl;
}

int main()
{
  srand(time(nullptr));
  testSommeVect2();
  // testSommeN();
  photoMap photos;
  photographie a({1, 2, 3});
  photos["test 1"] = a;
  a[0] = 7;
  photos["test 7"] = a;
  affichePhotos(photos);
  cout << a2("whello world !", 'w');
  return 0;
}
